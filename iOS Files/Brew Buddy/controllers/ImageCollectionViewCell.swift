 //
 //  ImageCollectionViewCell.swift
 //  Brew Buddy
 //
 //  Created by Alex Cevicelow on 4/1/19.
 //  Copyright © 2019 Alex Cevicelow. All rights reserved.
 //
 
 import UIKit
 
 class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgImage: UIImageView!
    
    override var isSelected: Bool{
        didSet {
            if self.isSelected{
                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
            else {
                self.transform = CGAffineTransform.identity
                self.contentView.backgroundColor = UIColor.white
            }
        }
    }
 }

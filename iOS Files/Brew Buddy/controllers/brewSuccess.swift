//
//  brewSuccess.swift
//  Brew Buddy
//
//  Created by Alex Cevicelow on 4/10/19.
//  Copyright © 2019 Alex Cevicelow. All rights reserved.
//

import UIKit
import Firebase

class brewSuccess: UIViewController {
    var localDidReciveToken = String()
    var db: Firestore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityMonitor()
        firebaseConfig()
        didReceiveBrew()
        
    }
    
    private func firebaseConfig () {
        let settings = FirestoreSettings()
        Firestore.firestore().settings = settings
        db = Firestore.firestore()
    }
    
    private func didReceiveBrew() {
        db.collection("homeBase").document("78kBh2c2OeNN8aTazMmz")
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    self.presentAlert(withTitle: "Error", message: "\(error!)")
                    return
                }
                guard let data = document.data() else {
                    self.presentAlert(withTitle: "Error", message: "Please Try Again")
                    return
                }
                let checkingReceivedValue = data["didReceiveBrew"] as! Int
                if (checkingReceivedValue == 1) {
                    
                    let alertController = UIAlertController(title: "Success!", message: "Your Brew Is Now In Progress", preferredStyle: UIAlertController.Style.alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        self.performSegue(withIdentifier: "postBrewGoingToMain", sender: self)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
                else if (checkingReceivedValue != 1) {
                    self.presentAlert(withTitle: "Error", message: "please try again")
                    
                }
                else {
                    self.presentAlert(withTitle: "Error", message: "Error Communicating with device")
                }
                
        }
    }
}

# Brew Buddy

Brew Buddy is a software and hardware solution for controlling the Keurig coffee maker remotely through an iOS application.This project is built on 3 main technologies: iOS, Arduino & Firebase.



# What I Learned

**iOS**

 - login system with firebase
 - real time database with cloud firestore
 - loginless authentication based on device ID
 - auto layout through storyboards 
 - UI collection views 
 - creating a custom launch screen
 - installing and using cocoaPods

**Firebase**

 - configuring cloud firestore for iOS
 - installing firebase cocoaPods
 
## Data Model  
```mermaid
graph LR
A[iOS App ] -- Coffee Data --> B((Cloud Firestore ))
B --Coffee Data --> D{Arduino}
D --Status --> E((Cloud Firestore))  
E --Status Of Brew  --> A
